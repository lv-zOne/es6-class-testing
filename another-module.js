const IS_TEST_RUNNING = !!process.argv.find((arg) => arg.indexOf('mocha') > -1) || false; //detect of mocha tests are running or app is running, to suppress or not console.logs

module.exports = {
    logLinesAndText: (text) => { //console.log only function, but test with return result
        if (!IS_TEST_RUNNING) {
            console.log('\n------------------------------------------\n');
            console.log(text);
            console.log('\n------------------------------------------\n');
        } else {
            //even if a function is console only, ensure it returns value for the purpose of unit test assesment
            //use unique Symbol to encode both the function name and passed parameter so that we can check both in the unit test
            return Symbol.for('logLinesAndText|'+text); // if you are not comfortable using Symbol, you can always just return unique for the function string and check for it in the unit test result
        }
    },
    AnotherUtilityClass: class {
        static genRandomStr() { //dummy method returns random string
            return Math.random().toString(36).substring(2);
        }
    }
};