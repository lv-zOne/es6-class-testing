const {AnotherUtilityClass, logLinesAndText} = require('./another-module.js');

const GLOBAL_ONE = '|GLOBAL1|';
const GLOBAL_TWO = '|GLOBAL2|';
const IS_TEST_RUNNING = !!process.argv.find((arg) => arg.indexOf('mocha') > -1) || false; //detect of mocha tests are running or app is running, to suppress or not console.logs

class MyStringClass {
    constructor(inputStr1, inputStr2, concatStr, timeOfInstance) {
        this.inputStr1 = inputStr1;
        this.inputStr2 = inputStr2;
        this.concatStr = concatStr;
        this.timeOfInstance = timeOfInstance;

        this.resultStr = '';
        
        // if the imported logLinesAndText was called directly here (not via `this.` class wrapped method), 
        // it would have been very difficult to test that this run on constructor init / new instance
        // logLinesAndText('new instance created: ' + UtilClass.timeNow); //if you use this, you won't be able to unit test this being run for every instance
        this.logLinesAndText('new instance created: ' + UtilClass.timeNow);  //use this to be able to unit test this being run for every instance
        this.updateResultStrWithGlobals(); //method to be called in constructor - interesting case to unit test the constructor
    }

    //don't be confused by the same name, one is a property of the class this.logLinesAndText and the other is the external import logLinesAndText
    //if it does confuse you, you can always name the class one something else
    logLinesAndText(param) { //wrapped in a class method to allow unit testing the constructor
        return logLinesAndText(param); //call to external module method
    }

    get result() {
        return this.resultStr;
    }

    updateResultStrWithGlobals() {
        this.resultStr = GLOBAL_ONE + this.concatString() + GLOBAL_TWO;
    }

    concatString() {
        return this.inputStr1 + this.concatStr + AnotherUtilityClass.genRandomStr() + this.concatStr + this.inputStr2 + this.concatStr + this.timeOfInstance;
    }
}

class UtilClass {
    static whatTimeIsItNow() {
        return new Date().toISOString();
    }

    static get timeNow() {
        return this.whatTimeIsItNow();
    }

    static createTwoInstances() {
        const myStringObjA = new MyStringClass('Hello', 'World', ' | separator | ', this.whatTimeIsItNow());
        const myStringObjB = new MyStringClass('Another', 'World', ' | separator | ', this.whatTimeIsItNow());

        return {
            a: myStringObjA.result,
            b: myStringObjB.result
        };
    }
}

if (!IS_TEST_RUNNING) console.log(UtilClass.createTwoInstances());

module.exports = {
    MyStringClass: MyStringClass,
    UtilClass: UtilClass
};