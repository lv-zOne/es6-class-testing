const { expect } = require('chai');
const sinon = require('sinon');

const anotherModule = require('./another-module.js');
const {logLinesAndText, AnotherUtilityClass} = anotherModule;

describe('another-module.js Module', () => {
    it('should have 2 exports', () => {
        expect(Object.keys(anotherModule).length).to.equal(2);
    });

    it('should export AnotherUtilityClass', () => {
        expect(anotherModule.AnotherUtilityClass).to.be.a('function');
        expect(AnotherUtilityClass).to.be.a('function');
    });

    it('should export logLinesAndText', () => {
        expect(anotherModule.logLinesAndText).to.be.a('function');
        expect(logLinesAndText).to.be.a('function');
    });
});

describe('AnotherUtilityClass', () => {
    describe('method genRandomStr', () => {
        it('should return random string', () => {
            //It will be best if we check the integrity / validity of the random string gen method, as such we need to
            //stub Math.random to test our method
            sinon.stub(Math, 'random').returns(0.5021177737352187); //fixed random value every time
            //If we didn't stub Math.random the expect and equal statements which both rely on it, would be different
            //as when evaluated Math.random() would return two different values
            expect(AnotherUtilityClass.genRandomStr()).to.equal(Math.random().toString(36).substring(2));
            Math.random.restore();
        });
    });
});

describe('logLinesAndText', () => {
    it('should return Symbol', () => {
        //Since this method only outputs a console, and through our code
        //when testing is detected, as in when mocha runs, it returns a signature Symbol
        //which encompasses both the function name and the passed parameter value
        //this will be all we will test here
        const dummyParam = 'someText';
        const logLinesAndTextResult = logLinesAndText(dummyParam);
        expect(logLinesAndTextResult).to.equal(Symbol.for('logLinesAndText|'+dummyParam));
    });
});