# Demo Project highlighting Unit Testing ES6 Classes #

This project demonstrates how to solve various issues when testing **ES6 Classes, constructors, get and static methods, module imports and exports** (where we have classes and functions separated out as modules), **stubbing** what we're not interested in the respective unit tests, even **mocking Math.random() and Date.now()** when we need to run expect / equal assertions - two statement executions, and as such time sensitive to what Math.random() and Date methods return.

## Unit tests are run using: ##
* mocha - test framework - [mochajs.org](https://mochajs.org/)
* chai - assertion library - [chaijs.com](https://www.chaijs.com/)
* sinon - test spies, stubs and mocks - [sinonjs.org](https://sinonjs.org/)

### How do I get set up? ###

* Checkout
* use Node.js version 14+, as older versions break with the mocha version I've put in package.json, like this:
` `  
` `
```javascript
        node_modules/mocha/lib/esm-utils.js:6
            return import(url.pathToFileURL(file));
                   ^^^^^^
        
        SyntaxError: Unexpected token import
```
* Run `npm install` to resolve dependencies
* Run `npm start` to run the app
* Run `npm test` to run all tests

## Highlights from the code ##
* Detect if **mocha** is running so we can suppress in the code console outputs, to maintain beatufil mocha report, uninterrupted by console messages
` `  
` `
```javascript
        //detect of mocha tests are running or app is running, to suppress or not console.logs
        const IS_TEST_RUNNING = !!process.argv.find((arg) => arg.indexOf('mocha') > -1) || false; 
        //...
        if (!IS_TEST_RUNNING) console.log(UtilClass.createTwoInstances());


```
` `  

            Output:

```
        another-module.js Module
            ✓ should have 2 exports
            ✓ should export AnotherUtilityClass
            ✓ should export logLinesAndText
        
        AnotherUtilityClass
            method genRandomStr
            ✓ should return random string
        
        logLinesAndText
            ✓ should return Symbol
        
        app.js Module
            ✓ should have 2 exports
            ✓ should export MyStringClass 
            ✓ should export UtilClass
        
        MyStringClass
            constructor
            ✓ should set inputStr1 property for every instance
            ✓ should set inputStr2 property for every instance
            ✓ should set concatStr property for every instance
            ✓ should set timeOfInstance property for every instance
            ✓ should run logLinesAndText method for every instance
            ✓ should run updateResultStrWithGlobals method for every instance
            class method
            ✓ logLinesAndText should run imported logLinesAndText
            ✓ getter result should return resultStr
            ✓ updateResultStrWithGlobals should set resultStr based on concatString and global constants
            ✓ concatString should return a string based on AnotherUtilityClass.genRandomStr
        
        UtilClass
            ✓ static method whatTimeIsItNow should return Date ISO String
            ✓ static getter timeNow should return whatTimeIsItNow()
            ✓ static method createTwoInstances should return object with instances of MyStringClass
        
        21 passing (34ms)
```

* Stub **Date.now()** when we need to run expect / equal assertions - as we have two statement executions, and as such time sensitive to what Date methods return, so that it always returns the same result so that comparison/assertions can work.
` `  
` `
```javascript
        sinon.stub(Date, 'now').returns(new Date()); //fixed date
        //when new Date() is called (before we call toISOString on it), internally it will call Date.now that we stubbed
        expect(UtilClass.whatTimeIsItNow()).to.equal(new Date().toISOString());
        Date.now.restore();

        /*
        Note that in 'sinon.stub(...).returns(new Date())', 'new Date()' is executed and stored ONCE as a date object, 
        when stubbed, not rerun every time the stubbed Date.now is called.
        */
```

* Stub **Math.random()** when we need to run expect / equal assertions - as we have two statement executions, and as such time sensitive to what Math.random method returns, so that it always returns the same result so that comparison/assertions can work.
` `  
` `
```javascript
        sinon.stub(Math, 'random').returns(0.5021177737352187); //fixed random value every time
        //If we didn't stub Math.random the expect and equal statements which both rely on it, would be different
        //as when evaluated Math.random() would return two different values
        expect(AnotherUtilityClass.genRandomStr()).to.equal(Math.random().toString(36).substring(2));
        Math.random.restore();
```

* Check / verify **module exports** by length of imported module object (keys) and by the individual module class, function references
` `  
` `
```javascript
        const appModule = require('./app.js');
        const {MyStringClass, UtilClass} = appModule; //we need appModule separately for quick export/import testing

        //...

        describe('app.js Module', () => {
            it('should have 2 exports', () => {
                expect(Object.keys(appModule).length).to.equal(2);
            });

            it('should export MyStringClass', () => {
                expect(appModule.MyStringClass).to.be.a('function');
                expect(MyStringClass).to.be.a('function');
            });

            it('should export UtilClass', () => {
                expect(appModule.UtilClass).to.be.a('function');
                expect(UtilClass).to.be.a('function');
            });
        });
```

* Testing **constructor** related initialisation, when a new instance is created, namely all properties being set from the constructor function parameters, as well as verify if any methods are being called/run upon new instances. Accessing the prototype, and *stubbing the methods we want to detect as being called*.
` `  
` `
```javascript
        describe('MyStringClass', () => {
            describe('constructor', () => {

                //Property set check
                it('should set timeOfInstance property for every instance', () => {
                    myStringObjA = new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
                    expect(myStringObjA.timeOfInstance).to.be.a('string');
                });

                //Method run in constructor check
                it('should run updateResultStrWithGlobals method for every instance', () => {
                    //stub the method we need to check is called, inside the class prototype
                    const updateResultStrWithGlobalsStub = sinon.stub(MyStringClass.prototype, 'updateResultStrWithGlobals');
                    new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
                    sinon.assert.calledWith(updateResultStrWithGlobalsStub); //assert that the method is called
                });

            // ...
            });
        });

        /*
        Note that if you need to check an external (imported) method is being run on instantiation,
        the easiest thing is to wrap it in a class method, and use the class method in constructor 
        and in the unit test
        */ 

        //...
        const {logLinesAndText} = require('./another-module.js');

        //...
        class MyStringClass {
            constructor(inputStr1, inputStr2, concatStr, timeOfInstance) {
                this.logLinesAndText('new instance created: ' + UtilClass.timeNow);  //use this to be able to unit test this being run for every instance
                //it would have been more difficult to test what was run in the constructor if we had called directly the below
                // logLinesAndText('new instance created: ' + UtilClass.timeNow);
            }

            //don't be confused by the same name, one is a property of the class this.logLinesAndText and the other is the external import logLinesAndText
            //if it does confuse you, you can always name the class one something else
            logLinesAndText(param) { //wrapped in a class method to allow unit testing the constructor
                return logLinesAndText(param); //call to external module method
            }
            //...
        }
```
* If you need to **test a method that doesn't return anything for the purpose of the app code**, like writes to console only or to files, etc, you can still use a unique return from it, which won't affect the code, but will allow you to test it. What I've chosen to use is ES6 [Symbol.for()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol/for) and use a combination of the function name and the value of the parameter being passed, so that we can verify in one go in the unit test that the right method is called and also the passed parameters worked out.
` `  
` `
```javascript
        logLinesAndText: (text) => { //console.log only function, but test with return result
            if (!IS_TEST_RUNNING) {
                console.log('\n------------------------------------------\n');
                console.log(text);
                console.log('\n------------------------------------------\n');
            } else {
                //even if a function is console only, ensure it returns value for the purpose of unit test assesment
                //use unique Symbol to encode both the function name and passed parameter so that we can check both in the unit test
                return Symbol.for('logLinesAndText|'+text); 
                //if you are not comfortable using Symbol, you can always just return unique for the function string and 
                //check for it in the unit test result
            }
        }

        // and in the unit test:

        describe('logLinesAndText', () => {
            it('should return Symbol', () => {
                //Since this method only outputs a console, and through our code
                //when testing is detected, as in when mocha runs, it returns a signature Symbol
                //which encompasses both the function name and the passed parameter value
                //this will be all we will test here
                const dummyParam = 'someText';
                const logLinesAndTextResult = logLinesAndText(dummyParam);
                expect(logLinesAndTextResult).to.equal(Symbol.for('logLinesAndText|'+dummyParam));
            });
        });
```
* **Get/getter methods** that act as properties, can be tested as any other normal class method
` `  
` `
```javascript
        //app code
        static get timeNow() {
            return this.whatTimeIsItNow();
        }

        //unit test code
        it('static getter timeNow should return whatTimeIsItNow()', () => {
            sinon.stub(Date, 'now').returns(new Date()); //fixed date
            //as timeNow is a getter, no need to use () after timeNow, as it works as a property
            expect(UtilClass.timeNow).to.equal(UtilClass.whatTimeIsItNow());
            Date.now.restore();
        });
```
* **Static methods** get tested as normal methods too, with the difference that they are called on the class name and not the instance of it (no need for new ClassName(), instead - ClassName.methodName()), as this is the nature of static methods, most often used as utility methods that apply to all instances and not just curren one. See details about [static methods on MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/static)
` `  
` `
```javascript
        //app code
        static whatTimeIsItNow() {
            return new Date().toISOString();
        }

        //unit test code
        it('static method whatTimeIsItNow should return Date ISO String', () => {
            sinon.stub(Date, 'now').returns(new Date()); //fixed date
            expect(UtilClass.whatTimeIsItNow()).to.equal(new Date().toISOString());
            Date.now.restore();
        });
```