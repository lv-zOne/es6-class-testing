const { expect } = require('chai');
const sinon = require('sinon');

const appModule = require('./app.js');
const {MyStringClass, UtilClass} = appModule; //we need appModule separately for quick export/import testing
const {AnotherUtilityClass} = require('./another-module.js');

const GLOBAL_ONE = '|GLOBAL1|';
const GLOBAL_TWO = '|GLOBAL2|';

let instanceParamsA = ['Hello', 'World', ' | separator | ', UtilClass.whatTimeIsItNow()];
let instanceParamsB = ['Another', 'World', ' | separator | ', UtilClass.whatTimeIsItNow()];

describe('app.js Module', () => {
    it('should have 2 exports', () => {
        expect(Object.keys(appModule).length).to.equal(2);
    });

    it('should export MyStringClass', () => {
        expect(appModule.MyStringClass).to.be.a('function');
        expect(MyStringClass).to.be.a('function');
    });

    it('should export UtilClass', () => {
        expect(appModule.UtilClass).to.be.a('function');
        expect(UtilClass).to.be.a('function');
    });
});

describe('MyStringClass', () => {

    describe('constructor', () => {

        it('should set inputStr1 property for every instance', () => {
            myStringObjA = new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
            expect(myStringObjA.inputStr1).to.be.a('string');
        });

        it('should set inputStr2 property for every instance', () => {
            myStringObjA = new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
            expect(myStringObjA.inputStr2).to.be.a('string');
        });

        it('should set concatStr property for every instance', () => {
            myStringObjA = new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
            expect(myStringObjA.concatStr).to.be.a('string');
        });

        it('should set timeOfInstance property for every instance', () => {
            myStringObjA = new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
            expect(myStringObjA.timeOfInstance).to.be.a('string');
        });

        it('should run logLinesAndText method for every instance', () => {
            //stub the method we need to check is called, inside the class prototype
            const logLinesAndTextStub = sinon.stub(MyStringClass.prototype, 'logLinesAndText');
            new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
            sinon.assert.calledWith(logLinesAndTextStub); //assert that the method is called
        });

        it('should run updateResultStrWithGlobals method for every instance', () => {
            //stub the method we need to check is called, inside the class prototype
            const updateResultStrWithGlobalsStub = sinon.stub(MyStringClass.prototype, 'updateResultStrWithGlobals');
            new MyStringClass(...instanceParamsA); //instantiate, as such the constructor code would run
            sinon.assert.calledWith(updateResultStrWithGlobalsStub); //assert that the method is called
        });

        afterEach(() => {
            sinon.restore();
        });
    });

    describe('class method', () => {
        it('logLinesAndText should run imported logLinesAndText', () => {
            const myInstance = new MyStringClass(...instanceParamsA); //instantiate to access logLinesAndText class method (as it's not a static one) 

            const dummyParam = 'someText';
            const logLinesAndTextResult = myInstance.logLinesAndText(dummyParam);
            expect(logLinesAndTextResult).to.equal(Symbol.for('logLinesAndText|'+dummyParam)); //assess by returned Symbol which we generated in the externally imported function
        });

        it('getter result should return resultStr', () => { //getter unit tests are just like any other class method unit tests, just don't use the 'get' keyword, and access the name directly (as it it was a class property)
            const myInstance = new MyStringClass(...instanceParamsA); //instantiate to access logLinesAndText class method (as it's not a static one) 
        
            expect(myInstance.result).to.equal(myInstance.resultStr);
        }); 

        it('updateResultStrWithGlobals should set resultStr based on concatString and global constants', () => { //getter unit tests are just like any other class method unit tests, just don't use the 'get' keyword, and access the name directly (as it it was a class property)
            // we have to stub AnotherUtilityClass.genRandomStr and UtilClass.whatTimeIsItNow so they returns the same value in the multiple calls during assesment
            sinon.stub(AnotherUtilityClass, 'genRandomStr').returns('abcde'); //fixed string
            sinon.stub(UtilClass, 'whatTimeIsItNow').returns('2020-07-28T14:20:05.350Z'); //fixed string
            //Note that we're stubbing UtilClass.whatTimeIsItNow and not UtilClass.timeNow which is a getter and as such acts as property, and not method

            const myInstance = new MyStringClass(...instanceParamsA); //instantiate to access logLinesAndText class method (as it's not a static one)
            //To use the global constants values you have 2 options
            // * export them in module exports
            // * redefine them here in the spec file (the option I will go with)
            myInstance.updateResultStrWithGlobals();

            expect(myInstance.resultStr).to.equal(GLOBAL_ONE + myInstance.concatString() + GLOBAL_TWO);
            
            AnotherUtilityClass.genRandomStr.restore();
            UtilClass.whatTimeIsItNow.restore();
        });

        it('concatString should return a string based on AnotherUtilityClass.genRandomStr', () => { //getter unit tests are just like any other class method unit tests, just don't use the 'get' keyword, and access the name directly (as it it was a class property)
            // we have to stub AnotherUtilityClass.genRandomStr and UtilClass.whatTimeIsItNow so they returns the same value in the multiple calls during assesment
            sinon.stub(AnotherUtilityClass, 'genRandomStr').returns('abcde'); //fixed string
            sinon.stub(UtilClass, 'whatTimeIsItNow').returns('2020-07-28T14:20:05.350Z'); //fixed string
            //Note that we're stubbing UtilClass.whatTimeIsItNow and not UtilClass.timeNow which is a getter and as such acts as property, and not method

            const myInstance = new MyStringClass(...instanceParamsA); //instantiate to access logLinesAndText class method (as it's not a static one)
            const concatStringResult = myInstance.concatString();

            expect(concatStringResult).to.equal(myInstance.inputStr1 + myInstance.concatStr + AnotherUtilityClass.genRandomStr() + myInstance.concatStr + myInstance.inputStr2 + myInstance.concatStr + myInstance.timeOfInstance);
            
            AnotherUtilityClass.genRandomStr.restore();
            UtilClass.whatTimeIsItNow.restore();
        });
    });
});


describe('UtilClass', () => {
    it('static method whatTimeIsItNow should return Date ISO String', () => {
        //On a relatively fast computer, the time we call the expect and and equal
        //statements the new Date() will return the same
        //But to be perfectly precise, we need to stub Date.now to return static date
        //And new Date().toISOString uses Date.now()
        sinon.stub(Date, 'now').returns(new Date()); //fixed date
        expect(UtilClass.whatTimeIsItNow()).to.equal(new Date().toISOString());
        Date.now.restore();
    });

    it('static getter timeNow should return whatTimeIsItNow()', () => {
        //On a relatively fast computer, the time we call the expect and and equal
        //statements the new Date() will return the same
        //But to be perfectly precise, we need to stub Date.now to return static date
        //And new Date().toISOString uses Date.now()
        sinon.stub(Date, 'now').returns(new Date()); //fixed date
        expect(UtilClass.timeNow).to.equal(UtilClass.whatTimeIsItNow());
        Date.now.restore();
    });

    it('static method createTwoInstances should return object with instances of MyStringClass', () => {

        sinon.stub(AnotherUtilityClass, 'genRandomStr').returns('abcde'); //fixed string
        sinon.stub(UtilClass, 'whatTimeIsItNow').returns('2020-07-28T14:20:05.350Z'); //fixed string

        //reset using stubs
        instanceParamsA = ['Hello', 'World', ' | separator | ', UtilClass.whatTimeIsItNow()];
        instanceParamsB = ['Another', 'World', ' | separator | ', UtilClass.whatTimeIsItNow()];

        const createTwoInstancesResult = UtilClass.createTwoInstances();

        expect(createTwoInstancesResult.a).to.equal(new MyStringClass(...instanceParamsA).result);
        expect(createTwoInstancesResult.b).to.equal(new MyStringClass(...instanceParamsB).result);

        AnotherUtilityClass.genRandomStr.restore();
        UtilClass.whatTimeIsItNow.restore();
    });
});